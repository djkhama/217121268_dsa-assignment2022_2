import ballerinax/kafka;
import ballerina/io;
//import wso2/kafka;
import ballerina/log;
import ballerina/lang.'string;
import ballerina/time;

kafka:ConsumerConfiguration consumerConfiguration = {
    bootstrapServers: "localhost:9092",  //the bootstrap server is the list of remote server endpoints for kafka brokers
    groupId: "group-id",
    offsetReset: "earliest",
    // Subscribes to the topic ``.
    topics: ["DSA"] //change to the topic it needs to listen to

};

kafka:Consumer consumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

//add kafka producer here after consumer
kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);

public function main() returns error? {
    //befreo we call the consumer we need to send as a producer

    //Gets the current time of the system clock
    time:Utc utc1 = time:utcNow();

    json msg = {"Message Processed at": utc1 , ".. now sending reply": ""};
    byte[] serializedMsg = msg.toString().toByteArray("UTF-8");
    var result = kafkaProducer->send(serializedMsg, "DSA", partition = 0);
    
    if (result is error){
        io:println("Error while sending to kafka...");
    }else{
        io:println("message sent to kafka...");
    }

    // Polls the consumer for messages.
    kafka:ConsumerRecord[] records = check consumer->poll(1000);
    if (records is error) {
        log:printError("Error occurred while polling ", records);
    }

    foreach var kafkaRecord in records {
        byte[] messageContent = kafkaRecord.value;
        // Converts the `byte[]` to a `string`.
        string message = check string:fromBytes(messageContent);
        // we need to send a message here as a producer
        check kafkaProducer->send({
                                topic: "DSA2",  //must be a different topic name
                                value: message.toBytes()});
        // Prints the retrieved Kafka record.
        io:println("Topic: " , kafkaRecord.value , "Received Message: " + message);
    // }else{
    //     printError("Error occurred while converting message data", message);

    // }else {
    //     log: printError( "Error occurred while retrieving message data;" + "Unexpected type");

    // }

}
}
